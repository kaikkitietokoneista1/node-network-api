# node-network-api
Network (IP-lookup and more) API made with Node.JS.

## Demo/Website
srv.kaikkitietokoneista.net:8080

## Features

* All data are in JSON

* Find your IP-address
* Your IP-address geo data
* Given IP-address geo data
* Hostname to IP-address

## Installation

